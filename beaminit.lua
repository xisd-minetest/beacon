-- This file register beams nodes and particles
local colors = beacon.colors
local green_beam_climbable = beacon.config.green_beam_climbable
-- Register per color beam and base nodes
for _,color in ipairs(colors) do
	
	local beam_climbable = false
	if green_beam_climbable and color == 'green' then  beam_climbable = true end

	-- Base node
	minetest.register_node("beacon:"..color.."base", {
		visual_scale = 1.0,
		drawtype = "plantlike",
		tiles = {color.."base.png"},
		paramtype = "light",
		walkable = false,
		diggable = false,
		pointable = false,
		buildable_to = true,
		climbable = beam_climbable,
		light_source = 13,
		groups = {not_in_creative_inventory=1}
	})

	-- Beam node
	minetest.register_node("beacon:"..color.."beam", {
		visual_scale = 1.0,
		drawtype = "plantlike",
		tiles = {color.."beam.png"},
		paramtype = "light",
		walkable = false,
		diggable = false,
		pointable = false,
		buildable_to = true,
		climbable = beam_climbable,
		light_source = 13, -- max light_source = 14 (sun)
		groups = {not_in_creative_inventory=1}
	})

	-- Spawn Particles, regrow beam
	minetest.register_abm({
		nodenames = {"beacon:"..color.."base"}, 
		interval = 1,
		chance = 2,
		action = function(pos, node)
			-- Makes small particles emanate from the beginning of a beam
			-- --[[
			minetest.add_particlespawner(
				32, --amount
				4, --time
				{x=pos.x-0.25, y=pos.y-0.25, z=pos.z-0.25}, --minpos
				{x=pos.x+0.25, y=pos.y+0.25, z=pos.z+0.25}, --maxpos
				{x=-0.8, y=-0.8, z=-0.8}, --minvel
				{x=0.8, y=0.8, z=0.8}, --maxvel
				{x=0,y=0,z=0}, --minacc
				{x=0,y=0,z=0}, --maxacc
				0.5, --minexptime
				1, --maxexptime
				1, --minsize
				2, --maxsize
				false, --collisiondetection
				color.."particle.png" --texture
			)
			--]]
			-- Regrow the beam if nécessary
			-- --[[
			--]]
			--
			-- Place beam
			--
			local beam_break_nodes = beacon.config.beam_break_nodes

			for i=1,179 do
				local p = {x=pos.x, y=pos.y+i, z=pos.z}
				-- Get far node (from http://dev.minetest.net/minetest.get_node)
				local node = minetest.get_node(p)
				if node.name == "ignore" then
					minetest.get_voxel_manip():read_from_map(p, p)
					node = minetest.get_node(p)
				end
				-- Stop when hitting something else than air and config option is so
				if not beam_break_nodes and node.name ~= "air" 
				 and node.name ~= "beacon:"..color.."beam" then
					break
				-- Place node
				else	minetest.add_node(p, {name="beacon:"..color.."beam"})
				end
			end

		end,
	})

	-- --[[ Beam removal abm
	minetest.register_abm({
		nodenames = {"beacon:"..color.."beam"}, 
		interval = 1,
		chance = 2,
		action = function(pos, node)
			--
			-- Auto Remove Beam
			--
			under_pos = {x=pos.x, y=pos.y-1, z=pos.z}
			local under_node = minetest.get_node(under_pos)
			-- if under_node and under_node.name == 'air' then
			if under_node and under_node.name ~= "beacon:"..color.."beam" 
			  and under_node.name ~= "beacon:"..color.."base" then
				minetest.set_node(pos, {name='air'})
			end
		end,
	})
	--]]


end 
