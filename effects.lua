-- This file contains fonctions used for beacons special effects
--
local S = beacon.intllib 
local timer_timeout = beacon.config.timer_timeout
local effects_radius = beacon.config.effects_radius
local msg_prefix = beacon.config.msg_prefix
local blue_field = beacon.config.blue_field or false
local beacon_distance_check = beacon.config.beacon_distance_check


-- beacon.allow_fly = function(player)
	-- return player:get_attribute("_beacon_green_fly")
-- end

--
-- Blue Field node
--
local blue_field_solid = beacon.config.blue_field_solid or false

minetest.register_node("beacon:bluefield", {
	drawtype = "glasslike",
	tiles = {"bluefield.png"},
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	diggable = false,
	light_source = 5,
	groups = {not_in_creative_inventory=1}
})

minetest.register_node("beacon:bluefield_solid", {
	drawtype = "glasslike",
	tiles = {"bluefield.png"},
	paramtype = "light",
	sunlight_propagates = true,
	walkable = true,
	diggable = false,
	light_source = 5,
	groups = {not_in_creative_inventory=1}
})

--
-- Blue field ABMs
--

minetest.register_abm({
	label = "Field expansion",
	nodenames = {"beacon:blue"},
	interval = 6,
	chance = 1, 	--50,
	catch_up = false,
	action = function(pos, node)
		
		if not blue_field then return end 
		
		-- Is the beacon working ?
		local meta = minetest.get_meta(pos)
		local is_active = meta:get_string('active')

		if is_active ~= 'yes' then return end 
	
		-- Limits of radius
		local xr = {}
		xr.min = pos.x - effects_radius
		xr.max = pos.x + effects_radius
		local yr = {}
		yr.min = pos.y - effects_radius
		yr.max = pos.y + effects_radius
		local zr = {}
		zr.min = pos.z - effects_radius
		zr.max = pos.z + effects_radius
		
		--
		-- Draw a blue field around 
		--
		local radius_table = {x=xr,y=yr,z=zr}
		local node = "beacon:bluefield"
		if blue_field_solid then node = "beacon:bluefield_solid" end
		draw_force_field(radius_table, node)
	end
})



--
-- Utility fonctions
--

-- Return table of players inside the beacons radius
function get_players_inside_radius(pos, radius)
	-- Function from dev.minetest.net/minetest.get_objects_inside_radius
	local all_objects = minetest.get_objects_inside_radius(pos, radius)
	local players = {}
	local _,obj
	for _,obj in ipairs(all_objects) do
		if obj:is_player() then
			table.insert(players, obj)
		end
	end
	return players
end

-- Place field nodes around a given area
function draw_force_field(r, field_node)
	for xi = r.x.min,r.x.max do
		for yi = r.y.min,r.y.max do
			for zi = r.z.min,r.z.max do
				--	
				-- Draws the force field	
				--
				if xi == r.x.min or xi == r.x.max
				 or zi == r.z.min or zi == r.z.max
				 or yi == r.y.min or yi == r.y.max
				 then
					local p = {x=xi,y=yi,z=zi}
					local node = minetest.get_node(p)
					if node.name == "ignore" then
						minetest.get_voxel_manip():read_from_map(p, p)
						node = minetest.get_node(p)
					end

					if node.name == "air" then
						minetest.place_node(p, {name=field_node})
					end	
				end	
			end
		end
	end --]]
end

-- Remove field nodes around a given area
function remove_force_field(r, field_node)
	for xi = r.x.min,r.x.max do
		for yi = r.y.min,r.y.max do
			for zi = r.z.min,r.z.max do
				--	
				-- Limits the force field	
				--
				if xi == r.x.min or xi == r.x.max
				 or zi == r.z.min or zi == r.z.max
				 or yi == r.y.min or yi == r.y.max
				 then
					local p = {x=xi,y=yi,z=zi}
					local node = minetest.get_node(p)
					if node.name == "ignore" then
						minetest.get_voxel_manip():read_from_map(p, p)
						node = minetest.get_node(p)
					end
					if node and node.name == field_node then
						minetest.set_node(p, {name='air'})
					end	
				end	
			end
		end
	end --]]
end


function init_area(pos, player, color, field_node, effect_radius)

	if not effect_radius then effect_radius = effects_radius end

	-- Return if placing inside the radius of another beacon
	if beacon_distance_check and beacon.is_near(pos) then return end
	
	-- Set placer as meta
	local meta = minetest.get_meta(pos)
	local name = player:get_player_name()
	meta:set_string('placer', name)
	meta:set_string('color', color)
	meta:set_string('field_node', field_node)
	meta:set_string('effect_radius', effect_radius)
	
	-- Limits of radius
	local xr = {}
	xr.min = pos.x - effect_radius
	xr.max = pos.x + effect_radius
	local yr = {}
	yr.min = pos.y - effect_radius
	yr.max = pos.y + effect_radius
	local zr = {}
	zr.min = pos.z - effect_radius
	zr.max = pos.z + effect_radius
	
	--
	-- Prevent areas intersection
	--
	local pos_min = {x=xr.min, y=yr.min,z=zr.min}
	local pos_max = {x=xr.max, y=yr.max,z=zr.max}
	local conflict = false

	if minetest.get_modpath('areas') then
		-- Get data of all intersecting areas
		-- Retrun [area_id] => area_data (table)	
		local intersect = areas:getAreasIntersectingArea(pos_min, pos_max)
		-- Loop throug table element
		for area in pairs(intersect) do
			conflict = true
			break
		end
	end

	-- TODO: Detect conflict if area is not used
	-- (or if no protection because no areas priv)

  -- TODO: maybe use a more subtle approch
	
	if conflict then
		local meta = minetest.get_meta(pos)
		meta:set_string('infotext', "The surrounding area cannot be protected because one or more other protected area are present in the effect radius.")
		beacon.on_destruct(pos) -- Call beacon.on_destruct to remove the beam
		return
	end
	--
	-- Draw a field around 
	--
	local radius_table = {x=xr,y=yr,z=zr}
	if field_node then 
		draw_force_field(radius_table, node)
  end
	
	--
	-- Protect the area 
	--
  local area_id
	if minetest.get_modpath('areas') then
		local area_name = color.. " beacon at "..pos.x.." "..pos.y.." "..pos.z
		local p1 = { x = xr.max, y = yr.max, z = zr.max	}
		local p2 = { x = xr.min, y = yr.min, z = zr.min	}

		local canAdd, errMsg = areas:canPlayerAddArea(p1, p2, name)
		if not canAdd then
			minetest.chat_send_player(name, msg_prefix.."You can't protect that area: "..errMsg)
		else
			p1, p2 = areas:sortPos( p1, p2 );
			area_id = areas:add(name, area_name, p1, p2, nil)
			areas:save()
			minetest.chat_send_player(name, msg_prefix.."Your ".. color .." beacon is affecting the surrounding area.")
			meta:set_int("area_id", area_id)
		end	
	end
	
	-- TODO : Add formspec for area config
	
	-- Mark as active
	meta:set_string('active','yes')
  
  return area_id
end

function remove_area(pos, color, field_node,  effect_radius)

	local meta = minetest.get_meta(pos)
	if not name then name = meta:get_string('placer') end
	if not color then color = meta:get_string('color') end
	if not field_node then field_node = meta:get_string('field_node') end
	if not effect_radius then effect_radius = meta:get_string('effect_radius') or effect_radius end

	--
	-- Unprotect the area
	--
	if minetest.get_modpath('areas') then
		local id = meta:get_int("area_id")
		if id and id ~= 0 then
      if minetest.get_modpath('creative_areas') then
        creative_areas.remove(name, id)
      end
			areas:remove(id)
			areas:save()
			if name then minetest.chat_send_player(name, msg_prefix.."Removed area "..id) end
		end
	end
	
	--
	-- Remove the blue field
	--
	if field_node then 
		-- Limits of radius
		local xr = {}
		xr.min = pos.x - effect_radius
		xr.max = pos.x + effect_radius
		local yr = {}
		yr.min = pos.y - effect_radius
		yr.max = pos.y + effect_radius
		local zr = {}
		zr.min = pos.z - effect_radius
		zr.max = pos.z + effect_radius
		
		local radius_table = {x=xr,y=yr,z=zr}

		-- Remove field	
		remove_force_field(radius_table, field_node)
	end
end

--
-- Special effects
--

--
-- Red Beacon : Regenerates health
--
beacon.effects.red = {}
beacon.effects.red.on_timer = function(pos, elapsed)
	local players = get_players_inside_radius(pos, effects_radius)
	for _,player in ipairs(players) do
		local hp = player:get_hp()
		local hp_max = 20 -- FIXME: get hp_max from player properties
		if hp < hp_max then player:set_hp(hp+(0.5*2)) end
	end

	-- Restart timer
	local timer = minetest.get_node_timer(pos)
	timer:start(timer_timeout)
end
--
-- TODO: Purple Beacon : Double tools strength and regen
--
beacon.effects.purple = {}
beacon.effects.purple.on_timer = function(pos, elapsed)
	return
end 

--
-- Blue Beacon : Protect the area and draw blue field around
--
beacon.effects.blue = {}
beacon.effects.blue.after_place_node = function(pos, placer, itemstack, pointed_thing)

  local field_node = nil
  if blue_field then 
		local field_node = "beacon:bluefield"
		if blue_field_solid then field_node = "beacon:bluefield_solid" end
	end

  area_id = init_area(pos, placer, "blue", field_node, effect_radius)

end
beacon.effects.blue.on_destruct = function(pos)

	remove_area(pos)
	--
	-- Rest of on_destruct function common with other beacons
	--
	beacon.on_destruct(pos)
end
beacon.effects.blue.on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)

	local player_name = clicker:get_player_name()
	
	if minetest.get_modpath('areas') then
		-- Get area Id
		local meta = minetest.get_meta(pos)
		local id = meta:get_int("area_id")
		
		if not id then return end
		local areainfo = areas.areas[id]
		if not areainfo then return end

		local stat_msg = S("The blue beacon protect the area")
		local lab_name = S("Area Name : ")
		local lab_owner = S("Area Owner : ")
		local lab_friend = S("Allowed friends List : ")
		local lab_protect = S("Protection Active")
		local lab_field = S("Blue field around the limit of the area")
		local lab_save = S("Save changes")
		local lab_cancel = S("Cancel")
		local area_name = areainfo.name
		local area_owner = areainfo.owner
		local area_protect, area_friends, area_field 
		
		if areainfo.open and areainfo.open == "open" then
			 area_protect = "false"
		else area_protect = "true"
		end
		
		print(dump(areainfo))
		area_friends = ""
		area_field = tostring(blue_field)
		
		local formspec = "size[7,8]"..
				default.gui_bg..
				default.gui_bg_img..

				-- Status of the beacon protection
				"label[0,0;"..stat_msg.."]"..

				-- Name of the area
				-- Status of the beacon protection
				-- "label[0.4,0;"..stat_msg.."]"..
				"field[0.5,1.5;4,0.5;area_name;"..lab_name..";"..area_name.."]"..

				-- Owner of the area
				"field[0.5,2.7;4,0.5;area_owner;"..lab_owner..";"..area_owner.."]"..

				-- Friends (allowed to edit area)
				"field[0.5,3.9;4,0.5;area_friends;"..lab_friend..";"..area_friends.."]"..

				-- Size of the area
				--""..

				-- Tickbox protected or not
				"checkbox[0,5;area_protect;"..lab_protect..";"..area_protect.."]"..
				-- Tickbox protected or not
				"checkbox[0,5.5;area_field;"..lab_field..";"..area_field.."]"..

				-- button save changes
				--""..

				"button_exit[0.5,7;3,1;save;"..lab_save.."]"..
				"button_exit[4,7;2,1;cancel;"..lab_cancel.."]"

		minetest.show_formspec(player_name, "beacon:form", formspec)

	else
		local msg_noarea = S("Without the area protection mod, the blue beacon delimits an area without effectivly protecting it")
		minetest.chat_send_player(player_name, msg_noarea)
	end
	

end
			
--
-- Green Beacon : Allow flying in the area
--
beacon.effects.green = {}
beacon.effects.green.after_place_node = function(pos, placer, itemstack, pointed_thing)

  local field_node = nil
  -- if green_field then 
		-- local field_node = "beacon:greenfield"
		-- if green_field_solid then field_node = "beacon:greenfield_solid" end
	-- end

  area_id = init_area(pos, placer, "green", field_node, effect_radius)
  if area_id and minetest.get_modpath('creative_areas') then
  	local name = placer:get_player_name()
    creative_areas.add(name, area_id, {["interact"] = true, ["fly"] = true })
  end
end
beacon.effects.green.on_destruct = function(pos)

	remove_area(pos)
	--
	-- Rest of on_destruct function common with other beacons
	--
	beacon.on_destruct(pos)
end

-- Use globalstep instead of node timer for green beacon
-- beacause node can be in an unloaded area
--

-- Old globalstep, now using creative area mod
--[[
local timer = 0
minetest.register_globalstep(function(dtime)
	-- Update timer
	timer = timer + dtime
	if (timer >= timer_timeout) then
		
		-- List all connected player
		-- (to apply green beacon effect)
		local players = minetest.get_connected_players()
		for _,player in ipairs(players) do

			-- Get player infos
			local pos = player:getpos()
			local name = player:get_player_name()
			
			-- Don't bother if player is already able to edit privs
			if not minetest.check_player_privs(name, {privs = true}) then 

				-- Get player privs list
				local privs = minetest.get_player_privs(name)
				-- Does player have fly priv ?
				local player_has_privs = privs.fly
				
				-- Find beacons in radius
				local green_beacon_near = minetest.find_node_near(pos, effects_radius, {"beacon:greenbase"})
				
				-- Grant privs if found
				if green_beacon_near and not player_has_privs then 
					privs.fly = true
					player:set_attribute("_fly_effect_is_on.beacon",'true')
					minetest.set_player_privs(name, privs)
					minetest.chat_send_player(name, msg_prefix.."Proximity of a green beacon grant you the ability you to fly.")

				-- Revoke privs if not found
				elseif player_has_privs and not green_beacon_near then
					-- Remove allow fly tag
					player:set_attribute("_fly_effect_is_on.beacon",nil)
					
					-- Also don't break effect from cooking_potions mod
					local flying_allowed_anyways = ( cooking_potions and cooking_potions.allow_fly(player) )

					-- Revoke priv if no running effect
					if not flying_allowed_anyways then
						local t = timer_timeout - 0.2 
						-- Wait before end of the timeout to
						-- prevent player crashing before the next check
						minetest.after(t, function(privs)  
							privs.fly = nil			-- revoke priv
							minetest.set_player_privs(name, privs)
							minetest.chat_send_player(name, msg_prefix.."Far from the green beacon, you lost the ability to fly.")
						end, privs)
					end
				end
			end
		end
		-- Restart timer
		timer = 0
	end
end)

]]--
